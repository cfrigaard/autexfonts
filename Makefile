TARGET=pdf# oneof: dvi,ps,pdf
DOC   =aufonts
DOCS  =$(DOC)
PWD   =AUTEXFONTS#$(shell pwd)

.PHONY: all
all:
	@ echo "Run make aufont.pdf, aufonts, installtex, getexternals, tar or clean"
	@ echo " see maketruetypefonts for details "

$(DOC).pdf: $(DOC).tex Makefile

%.pdf: %.tex Makefile 
	pdflatex -interaction=nonstopmode $< 

.PHONY: aufonts
aufonts:
	./batchmakeaufonts

.PHONY: installtex
installtex:
	sudo apt install texlive texlive-latex-recommended texlive-latex-extra

.PHONY: getexternals
getexternals:
	mkdir -p Etc
	cd Etc && wget http://medarbejdere.au.dk/fileadmin/www.designmanual.au.dk/hent_filer/hent_skrifttyper/fonte.zip
	cd Etc && wget https://devnotcorp.wordpress.com/2011/06/10/use-truetype-font-with-pdflatex/
	cd Etc && wget http://www.radamir.com/tex/ttf-tex.htm
	cd Etc && wget https://www.tug.org/fontname/fontname.pdf

.PHONY: tar
tar:
	cd .. && tar --exclude=Etc --exclude='*~' -czf autexfonts.tgz ${PWD}  

.PHONY: clean
clean:
	- @ rm -f out.txt *.vrb *.nav *.out *.snm *.log *.err *.dvi *.aux *.toc *.idx *.ilg *.ind *.pdf *.ps *.bbl *.blg *.backup
