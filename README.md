AUTEXFONTS

AUTHOR : Carsten Eie Frigaard
VERSION: 0.1
DATE   : 10 Aug 2017
LICENSE: see LICENSE file

Adds custom fonts for Aarhus University to TeX and LaTeX.


PREREQUISITES

* Installed Texlive system. Not tested on other TeX
  distributions.

* Installed TeXMF (normally part of a TeX distribution.)

* Installed versions of ttf2afm, ttf2tfm, vptovf font 
  conversion tools.


INSTALL THE AU FONTS

Run 
   
  > sudo ./batchmakeaufonts

You need sudo privilege to write to '/usr/local/share/texmf'
and the config file '/etc/texmf/web2c/updmap.cfg'.

The script will check for prerequisites and fail if any 
error occurs (and stop futher processing).

Standard output placed in 'batchmakeaufonts.out',
standard errors placed in 'batchmakeaufonts.err'.


MAKEFILE

Mostly for internal test, not intended for end users.

You could try 
   
  > make getexternals

to fetch a number of important TeX and Metafont documents.
  

FONT INSTALLED 

AU Passata Regular
  laupassatarg

AU Passata Bold
  laupassatabold

AU Passata Light
  laupassatalight 

AU Passata Light Bold}
  laupassatalightbold

AU Peto
  laupeto

TEXMF FILE PLACEMENT

Converted TrueType fonts placed in /usr/local/share/texmf

  /usr/local/share/texmf/
  ├── fonts
  │   ├── map
  │   │   └── dvips
  │   │       ├── aulogobold
  │   │       │   └── aulogobold.map
  │   │       ├── aulogoreg
  │   │       │   └── aulogoreg.map
  │   │       ├── aupassatabold
  │   │       │   └── aupassatabold.map
  │   │       ├── aupassatalight
  │   │       │   └── aupassatalight.map
  │   │       ├── aupassatalightbold
  │   │       │   └── aupassatalightbold.map
  │   │       ├── aupassatarg
  │   │       │   └── aupassatarg.map
  │   │       └── aupeto
  │   │           └── aupeto.map
  │   ├── tfm
  │   │   └── au
  │   │       ├── aulogobold
  │   │       │   ├── aulogoboldb8t.tfm
  │   │       │   └── aulogoboldbo8t.tfm
  │   │       ├── aulogoreg
  │   │       │   ├── aulogoregr8t.tfm
  │   │       │   └── aulogoregro8t.tfm
  │   │       ├── aupassatabold
  │   │       │   ├── aupassataboldb8t.tfm
  │   │       │   └── aupassataboldbo8t.tfm
  │   │       ├── aupassatalight
  │   │       │   ├── aupassatalightr8t.tfm
  │   │       │   └── aupassatalightro8t.tfm
  │   │       ├── aupassatalightbold
  │   │       │   ├── aupassatalightboldb8t.tfm
  │   │       │   └── aupassatalightboldbo8t.tfm
  │   │       ├── aupassatarg
  │   │       │   ├── aupassatargr8t.tfm
  │   │       │   └── aupassatargro8t.tfm
  │   │       └── aupeto
  │   │           ├── aupetor8t.tfm
  │   │           └── aupetoro8t.tfm
  │   └── truetype
  │       └── au
  │           ├── aulogobold
  │           │   ├── aulogoboldbo.ttf
  │           │   └── aulogoboldb.ttf
  │           ├── aulogoreg
  │           │   ├── aulogoregro.ttf
  │           │   └── aulogoregr.ttf
  │           ├── aupassatabold
  │           │   ├── aupassataboldbo.ttf
  │           │   └── aupassataboldb.ttf
  │           ├── aupassatalight
  │           │   ├── aupassatalightro.ttf
  │           │   └── aupassatalightr.ttf
  │           ├── aupassatalightbold
  │           │   ├── aupassatalightboldbo.ttf
  │           │   └── aupassatalightboldb.ttf
  │           ├── aupassatarg
  │           │   ├── aupassatargro.ttf
  │           │   └── aupassatargr.ttf
  │           └── aupeto
  │               ├── aupetoro.ttf
  │               └── aupetor.ttf
  ├── ls-R
  └── tex
      └── latex
          └── au
              ├── aulogobold
              │   └── T1aulogobold.fd
              ├── aulogoreg
              │   └── T1aulogoreg.fd
              ├── aupassatabold
              │   └── T1aupassatabold.fd
              ├── aupassatalight
              │   └── T1aupassatalight.fd
              ├── aupassatalightbold
              │   └── T1aupassatalightbold.fd
              ├── aupassatarg
              │   └── T1aupassatarg.fd
              └── aupeto
                  └── T1aupeto.fd

For the AU fonts to be avalible in TeX and LaTeX following
TeXMF commands are needed

  # Updating TeX filename database.
  > sudo texhash ${TEXMF}

  # Registering font mapping."
  > updmap-sys --enable Map=${FONTNAME}.map

and your system TeXMF config (file updmap.cfg) should now 
have been updated to

  > cat /etc/texmf/web2c/updmap.cfg 
  Map aulogobold.map
  Map aulogoreg.map
  Map aupassatabold.map
  Map aupassatalight.map
  Map aupassatarg.map
  Map aupassatalightbold.map
  Map aupeto.map
 

NOTES ON TeX AND LaTeX FONT NAMING

The naming convention for TeX and especially LaTeX are
rather complicated, see document 'Etc/fontname.pdf' 
[https://www.tug.org/fontname/fontname.pdf].

The LaTeX font map is found int the file font description 
map, say file
'/usr/local/share/texmf/tex/latex/au/aupassatarg/T1aupassatarg.fd'

  \ProvidesFile{T1aupassatarg.fd}[Font definitions for T1/aupassatarg.]
   
  \DeclareFontFamily{T1}{aupassatarg}{}
   
  \DeclareFontShape{T1}{aupassatarg}{m}{n}{<-> aupassatargr8t}{}
  \DeclareFontShape{T1}{aupassatarg}{m}{sc}{<-> aupassatargb8t}{}
  \DeclareFontShape{T1}{aupassatarg}{m}{sl}{<-> aupassatargro8t}{}
  \DeclareFontShape{T1}{aupassatarg}{m}{it}{<-> aupassatargri8t}{}
  \DeclareFontShape{T1}{aupassatarg}{b}{n}{<-> aupassatargb8t}{}
  \DeclareFontShape{T1}{aupassatarg}{b}{sc}{<-> aupassatargb8t}{}
  \DeclareFontShape{T1}{aupassatarg}{b}{sl}{<-> aupassatargbo8t}{}
  \DeclareFontShape{T1}{aupassatarg}{b}{it}{<-> aupassatargbi8t}{}
  \DeclareFontShape{T1}{aupassatarg}{sb}{n}{<->ssub * aupassatarg/b/n}{}
  \DeclareFontShape{T1}{aupassatarg}{sb}{sc}{<->ssub * aupassatarg/b/sc}{}
  \DeclareFontShape{T1}{aupassatarg}{sb}{sl}{<->ssub * aupassatarg/b/sl}{}
  \DeclareFontShape{T1}{aupassatarg}{sb}{it}{<->ssub * aupassatarg/b/it}{}
  \DeclareFontShape{T1}{aupassatarg}{bx}{n}{<->ssub * aupassatarg/b/n}{}
  \DeclareFontShape{T1}{aupassatarg}{bx}{sc}{<->ssub * aupassatarg/b/sc}{}
  \DeclareFontShape{T1}{aupassatarg}{bx}{sl}{<->ssub * aupassatarg/b/sl}{}
  \DeclareFontShape{T1}{aupassatarg}{bx}{it}{<->ssub * aupassatarg/b/it}{}
   
  \endinput



TESTING

Run 

  > pdflatex aufonts.tex

to see if the fresly installed fonts works in your system.

If fonts are not installed correctly you will get a 
'missfonts.log' file containing

  mktextfm laupassatargr8t
  mktextfm laupassataboldb8t
  mktextfm laupassatalightr8t
  mktextfm laupassatalightboldb8t
  mktextfm laupetor8t

